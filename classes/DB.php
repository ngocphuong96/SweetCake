<?php
 
class DB
{
    // Khai b�o c�c bi?n du?i d?ng private
      private $hostname = 'localhost',
            $username = 'root',
            $password = '',
            $dbname = 'webbanhngot';
    // Khai b�o c�c bi?n k?t n?i to�n c?c
    public $cn = NULL;
    public $rs = NULL;
 
    // H�m k?t n?i
    public function connect()
    {
        // K?t n?i
        $this->cn = mysqli_connect($this->hostname, $this->username, $this->password, $this->dbname);
        $cn = mysqli_connect($this->hostname, $this->username, $this->password, $this->dbname);

        mysqli_set_charset($cn, 'UTF8');
    }
 
    // H�m ng?t k?t n?i
    public function close(){
        // N?u d� k?t n?i
        if ($this->cn){
            // Ng?t k?t n?i
            mysqli_close($this->cn);
        }
    }
 
    // H�m truy v?n
    public function query($sql = null) 
    {       
        // N?u d� k?t n?i
        if ($this->cn){
            // Truy v?n
            mysqli_query($this->cn, $sql);
        }
    }
 
    // H�m d?m h�ng
    public function num_rows($sql = null) 
    {
        // N?u d� k?t n?i
        if ($this->cn)
        {
            $query = mysqli_query($this->cn, $sql);
            $row = mysqli_num_rows($query);
            return $row;
        }
    }
 
    // H�m l?y d? li?u
    public function fetch_assoc($sql = null, $type)
    {
        // N?u d� k?t n?i
        if ($this->cn)
        {
            // Th?c thi truy v?n
            $query = mysqli_query($this->cn, $sql);
            // N?u tham s? type = 0
            if ($type == 0)
            {
                while ($row = mysqli_fetch_assoc($query))
                {
                    $data[] = $row;
                }
                return $data;
            }
            // N?u tham s? type = 1
            else if ($type == 1)
            {
                $data = mysqli_fetch_assoc($query);
                return $data;
            }       
        }
    }
 
    // H�m x? l� chu?i d? li?u truy v?n
    public function real_escape_string($string) {
        // N?u d� k?t n?i
        if ($this->cn)
        {
            // X? l� chu?i d? li?u truy v?n
            $string = mysqli_real_escape_string($this->cn, $string);
        }
        // Ngu?c l?i chua k?t n?i
        else
        {
            $string = $string;
        }
        return $string;
    }
 
    // H�m l?y ID v?a insert
    public function insert_id() {
        // N?u d� k?t n?i
        if ($this->cn)
        {
            // L?y ID v?a insert
            return mysqli_insert_id($this->cn);
        }
    }
}
 
?>
