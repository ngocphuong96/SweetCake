<?php
 
class DB
{
    // Khai báo các biến dưới dạng private
      private $hostname = 'localhost',
            $username = 'root',
            $password = '',
            $dbname = 'webbanhngot';
    // Khai báo các biến kết nối toàn cục
    public $cn = NULL;
    public $rs = NULL;
 
    // H�m k?t n?i
    public function connect()
    {
        // Kết nối
        $this->cn = mysqli_connect($this->hostname, $this->username, $this->password, $this->dbname);
        $cn = mysqli_connect($this->hostname, $this->username, $this->password, $this->dbname);

        mysqli_set_charset($cn, 'UTF8');
    }
 
    //Hàm ngắt kết nối
    public function close(){
        // Nếu đã kết nối
        if ($this->cn){
            //Ngắt kết nối
            mysqli_close($this->cn);
        }
    }
 
    // Hàm truy vấn
    public function query($sql = null) 
    {       
        //Nếu đã kết nối
        if ($this->cn){
            // Truy vấn
            mysqli_query($this->cn, $sql);
        }
    }
 
    // Hàm đếm hàng
    public function num_rows($sql = null) 
    {
        //Nếu đã kết nối
        if ($this->cn)
        {
            $query = mysqli_query($this->cn, $sql);
            $row = mysqli_num_rows($query);
            return $row;
        }
    }
 
    // Hàm lấy dữ liệu
    public function fetch_assoc($sql = null, $type)
    {
        //Nếu đã kết nối
        if ($this->cn)
        {
            // Thực thi truy vấn
            $query = mysqli_query($this->cn, $sql);
            
            if ($type == 0)
            {
                while ($row = mysqli_fetch_assoc($query))
                {
                    $data[] = $row;
                }
                return $data;
            }
            
            else if ($type == 1)
            {
                $data = mysqli_fetch_assoc($query);
                return $data;
            }       
        }
    }
 
    // Hàm xử lí chuỗi dữ liệu truy vấn
    public function real_escape_string($string) {
        // Nếu đã kết nối
        if ($this->cn)
        {
            // Xử lí chuỗi dữ liệu truy v
            $string = mysqli_real_escape_string($this->cn, $string);
        }
        // Ngược lại chưa kết nối
        else
        {
            $string = $string;
        }
        return $string;
    }
 
    // Hàm lấy id vừa insert
    public function insert_id() {
        // Nếu đã kết nôti
        if ($this->cn)
        {
            // Lấy id vừa insert
            return mysqli_insert_id($this->cn);
        }
    }
}
 
?>
