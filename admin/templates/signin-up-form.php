

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <style type="text/css">
  </style>
</head>
<body>

<div class="container" style="padding-left: 20%">
<div id="loginbox"  class="col-md-8 col-sm-12" style="padding-top: 100px;">
                  
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title" style="text-align: center;">Đăng nhập</div>
                        
                    </div>     

                    <div style="padding-top:50px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <form method="POST" onsubmit="return false;" id="formSignin">
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="user_signin" type="text" class="form-control" name="user_signin" value="" placeholder="Tên">                                        
                            </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="pass_signin" type="password" class="form-control" name="pass_signin" placeholder="Mật khẩu">
                            </div>
                                      <button class="btn btn-primary" id="submit_signin">Đăng nhập</button>
                                      <br><br>
                                    <div class="alert alert-danger hidden"></div>
                                    
                     
                            </form>     
                        </div>                     
                    </div> 
                </div>
</div>
</body>
</html>


