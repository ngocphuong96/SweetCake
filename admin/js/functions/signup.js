// JavaScript Document
$('#submit_signup').on('click', function() {
    // G�n c�c gi� tr? trong form dang k� v�o c�c bi?n
    $user_signup = $('#user_signup').val();
    $pass_signup = $('#pass_signup').val();
    $repass_signup = $('#repass_signup').val();
 
    // N?u m?t trong c�c bi?n n�y r?ng
    if ($user_signup == '' || $pass_signup == '' || $repass_signup == '')
    {
        // Hi?n th? th�ng b�o l?i
        $('#formSignup .alert').removeClass('hidden');
        $('#formSignup .alert').html('Vui lòng nhập đủ thông tin bên trên');
    }
    // Ngu?c l?i
    else
    {
        // Th?c thi g?i d? li?u b?ng Ajax
        $.ajax({
            url : 'signup.php', // �u?ng d?n file nh?n d? li?u
            type : 'POST', // Phuong th?c g?i d? li?u
            // C�c d? li?u
            data : {
                user_signup : $user_signup,
                pass_signup : $pass_signup,
                repass_signup : $repass_signup
            // Th?c thi khi g?i d? li?u th�nh c�ng
            }, success : function(data) {
                $('#formSignup .alert').html(data);
            }
        });
    }
});