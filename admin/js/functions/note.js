// JavaScript Document
// B?t s? ki?n khi click v�o n�t t?o
$('#submit_create_note').on('click', function() {
    // G�n c�c gi� tr? trong form t?o note v�o c�c bi?n
    $title_create_note = $('#title_create_note').val();
    $body_create_note = $('#body_create_note').val();
    $ac = 'create'; // H�nh d?ng
 
    // N?u m?t trong c�c bi?n n�y r?ng
    if ($title_create_note == '' || $title_create_note == '')
    {
        // Hi?n th? th�ng b�o l?i
        $('#formCreateNote .alert').removeClass('hidden');
        $('#formCreateNote .alert').html('Vui l�ng di?n d?y d? th�ng tin b�n tr�n.');
    }
    // Ngu?c l?i
    else
    {
        // Th?c thi g?i d? li?u b?ng Ajax
        $.ajax({
            url : 'note.php', // �u?ng d?n file nh?n d? li?u
            type : 'POST', // Phuong th?c g?i d? li?u
            // C�c d? li?u
            data : {
                title_create_note : $title_create_note,
                body_create_note : $body_create_note,
                ac : $ac
            // Th?c thi khi g?i d? li?u th�nh c�ng
            }, success : function(data) {
                $('#formCreateNote .alert').removeClass('hidden');
                $('#formCreateNote .alert').html(data);
            }
        });
    }
	});
// B?t s? ki?n khi click v�o n�t S?a
$('#submit_edit_note').on('click', function() {
    // G�n c�c gi� tr? trong form t?o note v�o c�c bi?n
    $title_edit_note = $('#title_edit_note').val();
    $body_edit_note = $('#body_edit_note').val();
    $ac = 'edit'; // H�nh d?ng
    $id_edit_note = $('#id_edit_note').val(); // L?y ID trong field ?n
 
    // N?u m?t trong c�c bi?n n�y r?ng
    if ($title_edit_note == '' || $title_edit_note == '')
    {
        // Hi?n th? th�ng b�o l?i
        $('#formEditNote .alert').removeClass('hidden');
        $('#formEditNote .alert').html('Vui l�ng di?n d?y d? th�ng tin b�n tr�n.');
    }
    // Ngu?c l?i
    else
    {
        // Th?c thi g?i d? li?u b?ng Ajax
        $.ajax({
            url : 'note.php', // �u?ng d?n file nh?n d? li?u
            type : 'POST', // Phuong th?c g?i d? li?u
            // C�c d? li?u
            data : {
                title_edit_note : $title_edit_note,
                body_edit_note : $body_edit_note,
                ac : $ac,
                id_edit_note : $id_edit_note
            // Th?c thi khi g?i d? li?u th�nh c�ng
            }, success : function(data) {
                $('#formEditNote .alert').html(data);
            }
        });
    }
});
	$('#submit_delete_note').on('click', function() {
    $ac = 'delete'; // H�nh d?ng
    $id_edit_note = $('#id_edit_note').val(); // L?y ID trong field ?n
 
    // Th?c thi g?i d? li?u b?ng Ajax
    $.ajax({
        url : 'note.php', // �u?ng d?n file nh?n d? li?u
        type : 'POST', // Phuong th?c g?i d? li?u
        // C�c d? li?u
        data : {
            ac : $ac,
            id_edit_note : $id_edit_note
        // Th?c thi khi g?i d? li?u th�nh c�ng
        }, success : function(data) {
            $('#modalDeleteNote .alert').html(data);
        }
    });
	
});