<head>
    <meta charset="utf-8">
    <title>SweetCake</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<!--Less styles -->
   <!-- Other Less css file //different less files has different color scheam
	<link rel="stylesheet/less" type="text/css" href="themes/less/simplex.less">
	<link rel="stylesheet/less" type="text/css" href="themes/less/classified.less">
	<link rel="stylesheet/less" type="text/css" href="themes/less/amelia.less">  MOVE DOWN TO activate
	-->
	<!--<link rel="stylesheet/less" type="text/css" href="themes/less/bootshop.less">
	<script src="themes/js/less.js" type="text/javascript"></script> -->
	
<!-- Bootstrap style --> 
    <link id="callCss" rel="stylesheet" href="themes/bootshop/bootstrap.min.css" media="screen"/>
    <link href="themes/css/base.css" rel="stylesheet" media="screen"/>
<!-- Bootstrap style responsive -->	
	<link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
	<link href="themes/css/font-awesome.css" rel="stylesheet" type="text/css">
<!-- Google-code-prettify -->	
	<link href="themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
<!-- fav and touch icons -->
    <link rel="shortcut icon" href="themes/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="themes/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="themes/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="themes/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="themes/images/ico/apple-touch-icon-57-precomposed.png">
	<style type="text/css" id="enject"></style>
	<script type="text/javascript" src="jquery.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
  </head>

	
	<div class="well well-small" style="background-color: white;">
	<h1 style="margin-left: 30px;">Liên hệ</h1>
	
		<div class="span3">
		<h4>Địa chỉ</h4>
		<p>	334 Nguyễn Trãi, Thanh Xuân, Hà Nội
			<br/><br/>
			<a href="#">origato@gmail.com</a><br/>
			﻿Tel 123-456-6780<br/>
			Fax 123-456-5679<br/>
		</p>		
		</div>
			
		<div class="span3">
		<h4>Giờ mở cửa</h4>
			<h5> Monday - Friday</h5>
			<p>09:00am - 09:00pm<br/><br/></p>
			<h5>Saturday</h5>
			<p>09:00am - 07:00pm<br/><br/></p>
			<h5>Sunday</h5>
			<p>12:30pm - 06:00pm<br/><br/></p>
		</div>
		<div class="span2">
		<h4>Liên hệ</h4>
			<p><a href="#">Facebook: VitaDolce</a></p>
			<p><a href="#">Skype: VitaDolce</a></p>
		</div>
		
	<div class="row">
	<div class="span12">
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d16030.712590441344!2d105.80686117362883!3d20.99567332652532!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd740a66998e1a0ed!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBLaG9hIGjhu41jIFThu7Egbmhpw6pu!5e0!3m2!1svi!2s!4v1513789370326" width="70%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe><br />
	<small><a href="https://www.google.com/maps/place/Tr%C6%B0%E1%BB%9Dng+%C4%90%E1%BA%A1i+h%E1%BB%8Dc+Khoa+h%E1%BB%8Dc+T%E1%BB%B1+nhi%C3%AAn/@20.9956733,105.8068612,14.89z/data=!4m5!3m4!1s0x0:0xd740a66998e1a0ed!8m2!3d20.996017!4d105.807959" style="color:#0000FF;text-align:left">View Larger Map</a></small>
	
	</div>
	</div>
</div>
</div>
</div>
