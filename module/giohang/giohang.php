<div class="well well-small" style="background-color: white;">
<?php ob_start(); ?>
<h3>Danh sách bánh đã chọn</h3>
<?php
  if(isset($_POST['btn_capnhat'])){
    if(isset($_SESSION['cart'])&& !empty($_SESSION['cart'])){
        foreach($_SESSION['cart'] as $value){
          $name=$value['maHH'];
          $SL=$_POST[$name];
          settype($SL,"integer");
          $_SESSION['cart'][$name]['SL']=$SL;
        }
      }
  }
?>
<?php
  if(isset($_POST['btn_muatiep'])){
    header("Location: ./");
  }
?>

<?php
    if(isset($_SESSION['cart'])&& !empty($_SESSION['cart'])){
        foreach($_SESSION['cart'] as $value){
          $xoa="xoa_".$value['maHH'];
        if(isset($_POST[$xoa])){
          unset($_SESSION['cart'][$value['maHH']]);
          if(empty($_SESSION['cart'])){
              header("Location: ./");
            }
          }
          
        }
      }
  
?>
<?php
  if(isset($_POST['btn_huy'])){
    unset($_SESSION['cart']);
    header("Location: ./");
  }
?>

<form id="form1" name="form1" method="post" action="">
<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Tên món ăn</th>
      <th scope="col">Giá</th>
      <th scope="col">Số lượng</th>
      <th scope="col">Đơn vị tính</th>
      <th scope="col">Thành tiền</th>
      <th scope="col">Khác</th>
    </tr>
  </thead>
  
  <?php 
  $tongtien=0;
  if(isset($_SESSION['cart'])&& !empty($_SESSION['cart'])){
        foreach($_SESSION['cart'] as $value){
          $thanhtien=$value['SL']*$value['gia'];
          $tongtien+=$thanhtien;
    ?>
    <tr>
      <th><?php echo $value['ten_hang'] ?></th>
      <td><?php echo $value['gia']."$"?></td>
      <td><input type="text" name="<?php echo $value['maHH'] ?>" value="<?php echo $value['SL'] ?>" style="width: 50px;"></td>
      <td><?php echo $value['dvt'] ?></td>
      <td><?php echo number_format($thanhtien,0,"",".")."$" ?></td>
      <td><label>
        <input name="xoa_<?php echo $value['maHH'] ?>" type="submit" id="xoa" value="Xóa" class="btn btn-info" style="background: #BB2C60" />
        </label></td>
    </tr>
    <?php 
      } 
      
    } 
    $_SESSION['tongtien']=$tongtien;
  ?>
  
</table>
<div><label>Tổng tiền: <?php echo number_format($_SESSION['tongtien'],0,"",".")."$"; ?> </label></div>
<input name="btn_capnhat" type="submit" class="btn btn-info" id="btn_capnhat" value="Cập nhập" style="background: #BB2C60"/>
<input name="btn_muatiep" type="submit" class="btn btn-info" id="btn_muatiep" value="Mua tiếp" style="background: #BB2C60"/>
<input name="btn_huy" type="submit" class="btn btn-info" id="btn_huy" value="Hủy giỏ hàng" style="background: #BB2C60"/>
<a href="#btn_thanhtoan" role="button" data-toggle="modal" style="background: #BB2C60 "><span class="btn btn-info" style="background: #BB2C60">Thanh toán</span></a>
  <?php
    if(isset($_POST['btn_luu'])){
      $tenKH=$_POST['tenKH'];
      $ngay_sinh=$_POST['ngay_sinh'];
      $dia_chi=$_POST['dia_chi'];
      $SDT=$_POST['SDT'];
      $Email=$_POST['Email'];
      if(($tenKH=="")|| ($ngay_sinh=="")||($dia_chi=="")||($SDT=="")||($Email=="")){
        echo "<script>
          alert('Bạn phải điền đầy đủ thông tin');
        </script>";
      }else{
        $khach_hang="insert into khach_hang values(null,'$tenKH','$ngay_sinh','$dia_chi','$SDT','$Email')";
        $db->query($khach_hang);
        $maKH=$db->insert_id();
        $ngay_dat=date('Y-m-d H:i:s');
        $tong_tien=$_SESSION['tongtien'];
        $phieu_dat_hang="insert into phieu_dat_hang values(null,'$maKH','$tong_tien','0','$ngay_dat')";
        $db->query($phieu_dat_hang);
        $ma_phieu=$db->insert_id();
        if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){
          foreach ($_SESSION['cart'] as $value) {
            $maHH=$value['maHH'];
            $SL=$value['SL'];
            settype($SL,"integer");
            $chi_tiet_phieu_dat_hang="insert into chi_tiet_phieu_dat_hang values('$maHH','$ma_phieu','$SL')";
            $db->query($chi_tiet_phieu_dat_hang);
          }
        }
        unset($_SESSION['cart']);
        unset($_SESSION['tongtien']);
        echo "<script> 
          alert('đăng kí thành công');
          location.href='index.php';
         </script>";
      }
    }
  ?>

  <div id="btn_thanhtoan" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="thanhtoan" aria-hidden="false" style=" padding-left: 45px; padding-bottom: 10px; padding-top: 10px;" >
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 style="text-align: center;">Điền thông tin cá nhân</h3>
      </div>
      <div class="modal-body" style="margin-left: 10px; ">
      <form>
        <div class="form-group">
          <label>Họ tên:</label>
          <input type="text" class="form-control" id="tenKH" name="tenKH" placeholder="Họ tên" style="width: 450px;">
        </div>
        <div class="form-group">
          <label>Ngày sinh:</label>
          <input type="date" class="form-control" id="ngay_sinh" name="ngay_sinh" placeholder="Ngày_sinh" style="width: 450px;">
        </div>
        <div class="form-group">
          <label>Địa chỉ:</label>
          <input type="text" class="form-control" id="dia_chi" name="dia_chi" placeholder="Địa chỉ" style="width: 450px;">
        </div>
        <div class="form-group">
          <label>SĐT:</label>
          <input type="text" class="form-control" id="SDT" name="SDT" placeholder="Số điện thoại" style="width: 450px;">
        </div>
        <div class="form-group">
          <label >Email:</label>
          <input type="text" class="form-control" id="Email" name="Email" placeholder="Email" style="width: 450px;">
        </div>
        <button type="submit" class="btn" id="btn_luu" name="btn_luu" style="background: #BB2C60;color: white; margin-top: 7px;" >Lưu</button>
      <button class="btn" data-dismiss="modal" aria-hidden="true" style="background: #BB2C60; color: white; margin-top:  7px;">Hủy</button>
  </form>
      </div>
  </div>
  
  </form>
  
  
       