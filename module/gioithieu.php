<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>SweetCake</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<!--Less styles -->
   <!-- Other Less css file //different less files has different color scheam
	<link rel="stylesheet/less" type="text/css" href="themes/less/simplex.less">
	<link rel="stylesheet/less" type="text/css" href="themes/less/classified.less">
	<link rel="stylesheet/less" type="text/css" href="themes/less/amelia.less">  MOVE DOWN TO activate
	-->
	<!--<link rel="stylesheet/less" type="text/css" href="themes/less/bootshop.less">
	<script src="themes/js/less.js" type="text/javascript"></script> -->
	
<!-- Bootstrap style --> 
    <link id="callCss" rel="stylesheet" href="themes/bootshop/bootstrap.min.css" media="screen"/>
    <link href="themes/css/base.css" rel="stylesheet" media="screen"/>
<!-- Bootstrap style responsive -->	
	<link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
	<link href="themes/css/font-awesome.css" rel="stylesheet" type="text/css">
<!-- Google-code-prettify -->	
	<link href="themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
<!-- fav and touch icons -->
    <link rel="shortcut icon" href="themes/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="themes/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="themes/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="themes/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="themes/images/ico/apple-touch-icon-57-precomposed.png">
	<style type="text/css" id="enject"></style>
	<script type="text/javascript" src="jquery.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
  </head>
<body>
<div class="well well-small" style="background-color: white;">
<h3>Giới thiệu Origato</h3>
<img src="http://origato.vn/wp-content/uploads/2015/06/gioithieu.png" alt="" style="">
<p>
	Trong bối cảnh đất nước đổi mới, năm 1992, liên doanh sản xuất bánh kẹo đầu tiên ở miền Bắc, Hải Hà -Kotobuki, đã được thành lập trên cơ sở 71% số vốn góp của tập đoàn Kotobuki ( Nhật Bản ) và 29% vốn góp của công ty bánh kẹo Hải Hà ( Việt Nam) với thời hạn hoạt động 20 năm. Thời điểm đó, các công ty sản xuất bánh kẹo gặp rất nhiều khó khăn, máy móc thiết bị lạc hậu, nguồn vốn eo hẹp, trong khi đó nhu cầu của khách hàng ngày càng cao. Liên doanh với nước ngoài là một hướng hợp tác thích hợp với Hải Hà lúc đó. Với số vốn 4.000.000 USD đầu tư ban đầu, một số dây truyền sản xuất hiện đại và công nghệ tiên tiến của Nhật Bản đã được Kotobuki đưa vào Việt Nam cùng với khái niệm Marketing hay nói một cách khác đó là bài học... bán hàng hết sức lạ lẫm thời bấy giờ.
</p>
<br>
<p>
	Mặc dù ra đời từ năm 1960 nhưng đến trước thời điểm Kotobuki vào Liên doanh, khái niệm Marketing đối với doanh nghiệp trong nước hết sức mới lạ, chẳng ai có thể nghĩ rằng có thể đem hàng trăm thùng kẹo phát không cho khách hàng, kinh nghiệm của một doanh nghiệp nước ngoài đã hoạt động lâu năm với cơ chế thị trường giúp những người đại diện phần vốn phía Việt Nam nhận ra rằng hàng trăm thùng bánh kẹo phát không là rất nhỏ so với những gì thu được. Trên thị trường bắt đầu xuất hiện hàng chục sản phẩm mới, mẫu mã đẹp, chất lượng cao, giá cả phù hợp với thu nhập của người Việt Nam. Cái tên Hải Hà - Kotobuki dần trở lên quen thuộc với người tiêu dùng vời nhưng sản phẩm bim bim ( Snack), bánh Cookies, kẹo cứng nhân hoa quả, Socolate..... và đặc biệt là bánh ngọt, bánh gato danh tiếng.
</p>
<br>
<p>
	Sau thời kì đầu phát triển cùng với sự bùng nổ của thị trường bánh kẹo, vào những năm đầu của thế kỉ 21, Hải Hà - Kotobuki đã rơi vào giai đoạn khó khăn do chịu sự cạnh tranh quyết liệt của thị trường và cơ chế " bó" cảu liên doanh lúc đó. Trong khi đó các đối thủ cạnh tranh bắt đầu điều chỉnh chiến lược kinh doanh, đầu tư dây truyền công nghệ mới và cho ra đời các sản phẩm ưu thế vượt trội với sản phẩm Hải Hà - Kotobuki như: bánh Cookies Kinh Đô, Quảng Ngãi, Bibica, cốm Tràng An, Snack Oishi..... đã chiếm phần lớn thị phần bánh kẹo. Sau hơn 10 năm thành lập doanh thu của Hải Hà - Kotobuki mới đạt khoảng 60 tỉ đồng ( 2005), vấn đề thương hiệu và truyền thông thương hiệu chưa được chú trọng.
</p>
</div>
</body>
</html>
